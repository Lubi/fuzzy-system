Objašnjenje pokretanja aplikacije:
Potrebno je prvo napraviti .jar file od aplikacije.

Zatim je potrebno skinuti Fuzzy-system-simulator.rar it apendix direktorija i raspakirati.
Potrebno je u datoteci config.txt u prvom retku dodati apsolutni put do napravljene .jar datoteke.
U config.txt u 2. retku se odabire težina staze i gdje je i iz skupa {1,2,3}.
U config.txt u 3. retku se dodaje promjenjivost vjetra. Veći broj označava bržu promjenu jačine vjetra.
U 4. retku se namješta jačina vjetra.

Sada se možete igrati. :)
