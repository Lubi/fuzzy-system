package hr.fer.zemris.fuzzy.base.membershipFunctions;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class StandardFuzzySets {

    public StandardFuzzySets() {
    }

    public static IIntUnaryFunction lFunction(int lowerBound, int upperBound) {
        return x -> {
            if (x < lowerBound) {
                return 1;
            } else if (x < upperBound) {
                return ((double) upperBound - x) / (upperBound - lowerBound);
            } else return 0;
        };
    }

    public static IIntUnaryFunction gammaFunction(int lowerBound, int upperBound) {
        return x -> {
            if (x < lowerBound) {
                return 0;
            } else if (x < upperBound) {
                return ((double) x - lowerBound) / (upperBound - lowerBound);
            } else return 1;
        };
    }

    public static IIntUnaryFunction lambdaFunction(int lowerBound, int peak, int upperBound) {
        return x -> {
            if (x < lowerBound)
                return 0;
            else if (x < peak) {
                return ((double) x - lowerBound) / (peak - lowerBound);
            } else if (x < upperBound) {
                return ((double) upperBound - x) / (upperBound - peak);
            } else return 0;
        };
    }

    public static IIntUnaryFunction union(List<IIntUnaryFunction> unaryFunctions) {

        return xx ->
                unaryFunctions.stream().map(unaryFunction -> unaryFunction.valueAt(xx)).mapToDouble(Double::doubleValue).max().orElseThrow();

    }


}
