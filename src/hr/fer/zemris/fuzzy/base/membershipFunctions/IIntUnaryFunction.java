package hr.fer.zemris.fuzzy.base.membershipFunctions;

public interface IIntUnaryFunction {
    double valueAt(int x);
}
