package hr.fer.zemris.fuzzy.expression;

import hr.fer.zemris.fuzzy.input.InputParameters;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class CompositeExpression implements IExpression {
    List<SimpleExpression> expressions;

    public CompositeExpression(SimpleExpression ...  expressions) {
        this.expressions = new ArrayList<>(Arrays.asList(expressions));
    }

    @Override
    public List<Double> evaluate(InputParameters inputParameters) {
        List<Double> antecedentsMembership = new ArrayList<>();
        for (SimpleExpression exp : expressions){
            antecedentsMembership.add(exp.evaluate(inputParameters).get(0));
        }
        return antecedentsMembership;
    }
}
