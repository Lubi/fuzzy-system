package hr.fer.zemris.fuzzy.expression;

import hr.fer.zemris.fuzzy.input.InputParamType;
import hr.fer.zemris.fuzzy.input.InputParameters;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

import java.util.List;

public interface IExpression {
    static SimpleExpression is(InputParamType inputParamType, ILanguageVariable languageVariable){
        return new SimpleExpression(inputParamType, languageVariable);
    }

    List<Double>  evaluate(InputParameters inputParameters);

}
