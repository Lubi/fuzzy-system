package hr.fer.zemris.fuzzy.expression;

import hr.fer.zemris.fuzzy.input.InputParamType;
import hr.fer.zemris.fuzzy.input.InputParameters;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

import java.util.ArrayList;
import java.util.List;

public class SimpleExpression implements IExpression{
    InputParamType inputParamType;
    ILanguageVariable languageVariable;

    public SimpleExpression(InputParamType inputParamType, ILanguageVariable languageVariable) {
        this.inputParamType = inputParamType;
        this.languageVariable = languageVariable;
    }


    @Override
    public List<Double> evaluate(InputParameters inputParameters) {
        List< Double> memberships = new ArrayList<>();
        int valueOfInput = inputParameters.getValueForInputParamType(inputParamType);
        memberships.add(languageVariable.membership(valueOfInput));
        return memberships;


    }


}
