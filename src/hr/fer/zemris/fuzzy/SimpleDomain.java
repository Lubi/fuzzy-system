package hr.fer.zemris.fuzzy;

import java.security.InvalidParameterException;
import java.util.Objects;

public class SimpleDomain extends Domain {
    private int first;
    private int last;

    public SimpleDomain(int first, int last) {
        this.first = first;
        this.last = last;
    }

    @Override
    public int getCardinality() {
        return this.last - this.first;
    }

    @Override
    public IDomain getComponent(int ithDimension) {
        return this;
    }

    @Override
    public int getNumberOfComponents() {
        return 1;
    }


    public int getFirst() {
        return first;
    }

    public int getLast() {
        return last;
    }

    public int getIndexOfElementInDomain(int element){
        if (element < getFirst() || element > getLast()) throw new InvalidParameterException("element nije u domeni u kojoj ga trazis index");
        return element - getFirst();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SimpleDomain that = (SimpleDomain) o;
        return first == that.first &&
                last == that.last;
    }

    @Override
    public int hashCode() {
        return Objects.hash(first, last);
    }
}
