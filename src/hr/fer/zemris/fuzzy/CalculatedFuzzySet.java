package hr.fer.zemris.fuzzy;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;

public class CalculatedFuzzySet implements IFuzzySet {
    private IDomain fuzzySetDomain;
    private IIntUnaryFunction function;

    public CalculatedFuzzySet(IDomain domain, IIntUnaryFunction function) {
        fuzzySetDomain = domain;
        this.function = function;
    }

    @Override
    public IDomain getDomain() {
        return fuzzySetDomain;
    }

    @Override
    public double getValueAt(DomainElement domainElement) {
        return function.valueAt(fuzzySetDomain.indexOfElement(domainElement));
    }
}
