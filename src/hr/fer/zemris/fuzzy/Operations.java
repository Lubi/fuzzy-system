package hr.fer.zemris.fuzzy;

import java.security.InvalidParameterException;

public class Operations {

    public static IFuzzySet unaryOperation(IFuzzySet fuzzySet, IUnaryFunction unaryFunction) {
        MutableFuzzySet mutuableFuzzySet = new MutableFuzzySet(fuzzySet.getDomain());

        for (DomainElement domainElement : fuzzySet.getDomain()) {
            mutuableFuzzySet = mutuableFuzzySet.set(domainElement, unaryFunction.valueAt(fuzzySet.getValueAt(domainElement)));
        }
        return mutuableFuzzySet;
    }

    public static IFuzzySet binaryOperation(IFuzzySet firstFuzzySet, IFuzzySet secondFuzzySet, IBinaryFunction function) {
        if (!firstFuzzySet.getDomain().equals(secondFuzzySet.getDomain()))
            throw new InvalidParameterException("Domene moraju biti iste!");

        MutableFuzzySet mutuableFuzzySet = new MutableFuzzySet(firstFuzzySet.getDomain());

        for (DomainElement domainElement : firstFuzzySet.getDomain()) {
            mutuableFuzzySet = mutuableFuzzySet.set(domainElement, function.valueAt(firstFuzzySet.getValueAt(domainElement), secondFuzzySet.getValueAt(domainElement)));
        }
        return mutuableFuzzySet;
    }


    public static IUnaryFunction zadehNot() {
        return value -> 1 - value;
    }

    public static IBinaryFunction zadehAnd() {
        return Math::min;
    }

    public static IBinaryFunction zadehOr() {
        return Math::max;
    }

    public static IBinaryFunction hamacherTNorm(double v) {
        return (firstValue, secondValue) -> {
            if (v < 0) throw new InvalidParameterException("v koeficijent mora biti veci od 0");
            return (firstValue * secondValue) / (v + (1 - v) * (firstValue + secondValue - firstValue * secondValue));
        };
    }

    public static IBinaryFunction hamacherSNorm(double v) {
        return (firstValue, secondValue) -> {
            if (v < 0) throw new InvalidParameterException("v koeficijent mora biti veci od 0");
            return (firstValue + secondValue - (2 - v) * firstValue * secondValue) / (1 - (1 - v) * firstValue * secondValue);
        };
    }

}
