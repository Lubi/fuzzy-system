package hr.fer.zemris.fuzzy.defuzzyfication;

import hr.fer.zemris.fuzzy.IFuzzySet;
import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;

public interface IDefuzzifier {
    int defuzzyfy(IIntUnaryFunction unionOfConclusions);
}
