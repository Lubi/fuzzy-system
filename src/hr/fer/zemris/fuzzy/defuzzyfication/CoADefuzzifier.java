package hr.fer.zemris.fuzzy.defuzzyfication;

import hr.fer.zemris.fuzzy.DomainElement;
import hr.fer.zemris.fuzzy.IDomain;
import hr.fer.zemris.fuzzy.IFuzzySet;
import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.defuzzyfication.IDefuzzifier;

public class CoADefuzzifier implements IDefuzzifier {
    IDomain domain;

    public CoADefuzzifier(IDomain domain) {
        this.domain = domain;
    }

    @Override
    public int defuzzyfy(IIntUnaryFunction unionOfConclusions) {
        double sumOfWeights = 0;
        double sumWeightMulValue = 0;
        for (DomainElement x : domain){
            double membership = unionOfConclusions.valueAt(x.getComponentValue(0));
            sumOfWeights += membership;
            sumWeightMulValue += membership * x.getComponentValue(0);
        }
        return (int) (sumWeightMulValue / sumOfWeights); // (suma(membership * x) / suma(membership) )
    }
}
