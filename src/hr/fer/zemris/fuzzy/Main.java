/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package hr.fer.zemris.fuzzy;
import java.io.*;
import java.util.*;

import hr.fer.zemris.fuzzy.database.AccelerationDatabase;
import hr.fer.zemris.fuzzy.database.AngleDatabase;
import hr.fer.zemris.fuzzy.defuzzyfication.CoADefuzzifier;
import hr.fer.zemris.fuzzy.defuzzyfication.IDefuzzifier;
import hr.fer.zemris.fuzzy.implication.IImplication;
import hr.fer.zemris.fuzzy.implication.MinImplication;
import hr.fer.zemris.fuzzy.implication.ProductImplication;
import hr.fer.zemris.fuzzy.input.InputParamType;
import hr.fer.zemris.fuzzy.input.InputParameters;
import hr.fer.zemris.fuzzy.system.AccelerationFuzzySystem;
import hr.fer.zemris.fuzzy.system.AngleFuzzySystem;

public class Main {


    public static void main(String[] args) throws IOException, InterruptedException {
    	BufferedReader input = new BufferedReader(new InputStreamReader(System.in));

		IDomain accelerationDomain = Domain.intRange(-30, 30);
		IDomain angleDoamin = Domain.intRange(-90, 90);
//		IImplication min = new MinImplication();
		IImplication productImplication = new ProductImplication();

		AccelerationDatabase accelerationDatabase = new AccelerationDatabase();
		IDefuzzifier accelerationDeffuzzy = new CoADefuzzifier(accelerationDomain);
//		AccelerationFuzzySystem accelerationFuzzySystem = new AccelerationFuzzySystem(accelerationDatabase,accelerationDeffuzzy, min);
		AccelerationFuzzySystem accelerationFuzzySystem = new AccelerationFuzzySystem(accelerationDatabase,accelerationDeffuzzy, productImplication);

		AngleDatabase angleDatabase = new AngleDatabase();
		IDefuzzifier angleDefuzzyfier = new CoADefuzzifier(angleDoamin);
//		AngleFuzzySystem angleFuzzySystem = new AngleFuzzySystem(angleDatabase, angleDefuzzyfier, min);
		AngleFuzzySystem angleFuzzySystem = new AngleFuzzySystem(angleDatabase, angleDefuzzyfier, productImplication);



	    int L=0,D=0,LK=0,DK=0,V=0,S=0,akcel,kormilo;
	    String line = null;
		while(true){
			if((line = input.readLine())!=null){
				if(line.charAt(0)=='K') break;
				Scanner s = new Scanner(line);
				L = s.nextInt();
				D = s.nextInt();
				LK = s.nextInt();
				DK = s.nextInt();
				V = s.nextInt();
				S = s.nextInt();

				Map<InputParamType, Integer> params = new HashMap<>();
				params.put(InputParamType.LEFT, L);
				params.put(InputParamType.RIGHT, D);
				params.put(InputParamType.LEFT45, LK);
				params.put(InputParamType.RIGHT45, DK);
				params.put(InputParamType.VELOCITY, V);
				params.put(InputParamType.DIRECTION, S);
				InputParameters inputParameters = new InputParameters(params);

				akcel = accelerationFuzzySystem.conclude(inputParameters);
				kormilo = angleFuzzySystem.conclude(inputParameters);

				System.out.println(akcel + " " + kormilo);
				System.out.flush();

	        }


	   }
    }

}

