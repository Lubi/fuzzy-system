package hr.fer.zemris.fuzzy;

import java.security.InvalidParameterException;
import java.util.*;

public abstract class Domain implements IDomain {
    public Domain() {
    }

    public static IDomain intRange(int startNumber, int endNumber) {
        return new SimpleDomain(startNumber, endNumber);
    }

    public static IDomain combine(IDomain firstDomain, IDomain secondDomain) {
        List<SimpleDomain> combineDomains = new ArrayList<>();

        addToList(firstDomain, combineDomains);
        addToList(secondDomain, combineDomains);
        return new CompositeDomain(combineDomains);
    }

    private static void addToList(IDomain domain, List<SimpleDomain> combineDomains) {
        for (int i = 0; i < domain.getNumberOfComponents(); i++) {
            combineDomains.add((SimpleDomain) domain.getComponent(i));
        }
    }

    @Override
    public int indexOfElement(DomainElement domainElement) {
        int domainElementSize = domainElement.getNumberOfComponents();
        if (domainElementSize != this.getNumberOfComponents())
            throw new InvalidParameterException("dimenzije elementa kojem želite provjeriti indeks su različite od dimanzija domene! \n" +
                    "dimenzija vašeg elementa = " + domainElementSize +
                    "\ndimenzija domene je = " + this.getNumberOfComponents());

        int cardinalityForNow = 1;
        int resultIndex = 0;
        for (int i = domainElementSize - 1; i >= 0; i--) {
            SimpleDomain simpleDomain = (SimpleDomain) this.getComponent(i);
            int indexInSimpleDomain = domainElement.getComponentValue(i) - simpleDomain.getFirst();

            if (indexInSimpleDomain < 0 || indexInSimpleDomain >= simpleDomain.getCardinality())
                throw new ArrayIndexOutOfBoundsException("Izabrali ste element koji nije u domeni " + domainElement.toString());

            resultIndex += indexInSimpleDomain * cardinalityForNow;
            cardinalityForNow *= simpleDomain.getCardinality();
        }
        return resultIndex;
    }

    @Override
    public DomainElement elementForIndex(int indexOfElement) {
        if (indexOfElement < 0 || indexOfElement >= this.getCardinality())
            throw new ArrayIndexOutOfBoundsException("Izabrali ste indeks van dosega domene");
        int result = indexOfElement;
        int reminder;
        ArrayList<Integer> resultElements = new ArrayList<>();
        for (int i = getNumberOfComponents() - 1; i >= 0; i--) {
            reminder = result % getComponent(i).getCardinality();
            result = result / getComponent(i).getCardinality();
            resultElements.add(((SimpleDomain) getComponent(i)).getFirst() + reminder);
        }
        Collections.reverse(resultElements);
        return new DomainElement(resultElements.stream().mapToInt(Integer::intValue).toArray());
    }

    @Override
    public Iterator<DomainElement> iterator() {
        return new DomainIterator();
    }

    private class DomainIterator implements Iterator<DomainElement> {
        private int currentElement = 0;

        @Override
        public boolean hasNext() {
            return currentElement < getCardinality();
        }

        @Override
        public DomainElement next() {
            return elementForIndex(currentElement++);
        }
    }
}
