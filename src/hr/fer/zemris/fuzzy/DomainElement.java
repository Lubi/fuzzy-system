package hr.fer.zemris.fuzzy;

import java.util.Arrays;

public class DomainElement {
    private int[] values;

    public DomainElement(int[] values) {
        this.values = values;
    }

    public int getNumberOfComponents() {
        return this.values.length;
    }

    public int getComponentValue(int index) {
        return this.values[index];
    }

    public static DomainElement of(int... values) {
        return new DomainElement(values);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomainElement that = (DomainElement) o;
        return Arrays.equals(values, that.values);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(values);
    }

    @Override
    public String toString() {
        String[] strArray = Arrays.stream(values).mapToObj(String::valueOf).toArray(String[]::new);
        return "(" + String.join(",", strArray) + ")";
    }
}
