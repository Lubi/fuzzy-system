package hr.fer.zemris.fuzzy.rule;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.expression.IExpression;
import hr.fer.zemris.fuzzy.implication.IImplication;
import hr.fer.zemris.fuzzy.input.InputParameters;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

public class RuleBuilder {
    IExpression antecedent;
    ILanguageVariable goalLanguageVariable;

    public RuleBuilder(IExpression antecedent, ILanguageVariable goalLanguageVariable) {
        this.antecedent = antecedent;
        this.goalLanguageVariable = goalLanguageVariable;
    }

    public IIntUnaryFunction makeOneConclusion(InputParameters inputParameters, IImplication implication){
        return implication.implicationOperator(goalLanguageVariable, antecedent.evaluate(inputParameters));
    }
}
