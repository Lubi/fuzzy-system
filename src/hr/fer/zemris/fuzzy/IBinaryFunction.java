package hr.fer.zemris.fuzzy;

public interface IBinaryFunction {
    double valueAt(double firstValue, double secondValue);
}
