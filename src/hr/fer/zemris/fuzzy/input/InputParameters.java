package hr.fer.zemris.fuzzy.input;

import java.util.Dictionary;
import java.util.Map;

public class InputParameters {

    private Map<InputParamType, Integer> parameters;

    public InputParameters(Map<InputParamType, Integer> params) {
        this.parameters = params;
    }

    public Integer getValueForInputParamType(InputParamType paramType){
        return parameters.get(paramType);
    }


}
