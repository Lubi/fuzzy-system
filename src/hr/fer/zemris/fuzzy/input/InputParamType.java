package hr.fer.zemris.fuzzy.input;

public enum InputParamType {
    LEFT,
    RIGHT,
    LEFT45,
    RIGHT45,
    VELOCITY,
    DIRECTION;
}
