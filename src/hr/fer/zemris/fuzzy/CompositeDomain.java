package hr.fer.zemris.fuzzy;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class CompositeDomain extends Domain {
    private ArrayList<SimpleDomain> domains;

    public CompositeDomain(List<SimpleDomain> domains) {
        this.domains = new ArrayList<>(domains);
    }


    @Override
    public int getCardinality() {
        int compositeCardinality = 1;

        for (SimpleDomain domain : this.domains) {
            compositeCardinality *= domain.getCardinality();
        }
        return compositeCardinality;
    }

    @Override
    public IDomain getComponent(int ithDimension) {
        return domains.get(ithDimension);
    }

    @Override
    public int getNumberOfComponents() {
        return domains.size();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CompositeDomain that = (CompositeDomain) o;
        return Objects.equals(domains, that.domains);
    }

    @Override
    public int hashCode() {
        return Objects.hash(domains);
    }
}
