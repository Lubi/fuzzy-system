package hr.fer.zemris.fuzzy.system;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.base.membershipFunctions.StandardFuzzySets;
import hr.fer.zemris.fuzzy.database.AccelerationDatabase;
import hr.fer.zemris.fuzzy.database.AngleDatabase;
import hr.fer.zemris.fuzzy.defuzzyfication.IDefuzzifier;
import hr.fer.zemris.fuzzy.implication.IImplication;
import hr.fer.zemris.fuzzy.input.InputParameters;
import hr.fer.zemris.fuzzy.rule.RuleBuilder;

import java.util.ArrayList;
import java.util.List;

public class AngleFuzzySystem implements IFuzzySystem {
    AngleDatabase angleDatabase;
    IDefuzzifier defuzzifier;
    IImplication implication;

    public AngleFuzzySystem(AngleDatabase acceleationRuleBase, IDefuzzifier defuzzifier, IImplication implication) {
        this.angleDatabase = acceleationRuleBase;
        this.defuzzifier = defuzzifier;
        this.implication = implication;
    }

    @Override
    public int conclude(InputParameters inputParameters) {

        List<IIntUnaryFunction> conclusions = new ArrayList<>();

        for (RuleBuilder rule : angleDatabase.getRuls()){
            conclusions.add(rule.makeOneConclusion(inputParameters, implication));
        }
        return defuzzifier.defuzzyfy(StandardFuzzySets.union(conclusions));
    }
}