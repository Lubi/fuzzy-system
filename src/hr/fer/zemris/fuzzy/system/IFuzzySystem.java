package hr.fer.zemris.fuzzy.system;

import hr.fer.zemris.fuzzy.input.InputParameters;

public interface IFuzzySystem {
    int conclude(InputParameters inputParameters);
}
