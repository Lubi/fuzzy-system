package hr.fer.zemris.fuzzy;

import java.security.InvalidParameterException;
import java.util.ArrayList;
import java.util.Arrays;

public class Relations {

    public static boolean isUtimesURelation(IFuzzySet relation) {
        if (relation.getDomain().getNumberOfComponents() != 2)
            throw new InvalidParameterException("Za UxU potrebne su dvojke elemenata");

        return relation.getDomain().getComponent(0).equals(relation.getDomain().getComponent(1));
    }

    public static boolean isSymmetric(IFuzzySet relation) {
        if (!isUtimesURelation(relation))
            throw new InvalidParameterException("relacija nije UxU. To je potrebno za provjeru");

        IDomain relationDomain = relation.getDomain();
        int cardinalityOfU = relationDomain.getComponent(0).getCardinality(); //ista je kardinalnost obe komponente

        for (int i = 0; i < cardinalityOfU; i++) {
            for (int j = 0; j <= i; j++) {
                DomainElement elementFromDownTriangular = relationDomain.elementForIndex(i * cardinalityOfU + j);
                DomainElement elementFromUpperTriangular = relationDomain.elementForIndex(j * cardinalityOfU + i);
                if (Math.abs(relation.getValueAt(elementFromDownTriangular) - relation.getValueAt(elementFromUpperTriangular)) > 0.00001)
                    return false;
            }
        }
        return true;
    }


    public static boolean isReflexive(IFuzzySet relation) {
        if (!isUtimesURelation(relation))
            throw new InvalidParameterException("relacija nije UxU. To je potrebno za provjeru");

        IDomain relationDomain = relation.getDomain();
        int dimension = relationDomain.getComponent(0).getCardinality();

        for (int x = 0; x < dimension; x++) {
            DomainElement xxElement = relationDomain.elementForIndex(x * dimension + x);
            if (Math.abs(relation.getValueAt(xxElement) - 1) > 0.00001)
                return false;
        }
        return true;

    }


    public static boolean isMaxMinTransitive(IFuzzySet relation) {
        if (!isUtimesURelation(relation))
            throw new InvalidParameterException("relacija nije UxU. To je potrebno za provjeru");

        IDomain relationDomain = relation.getDomain();
        int cardinalityOfU = relationDomain.getComponent(0).getCardinality();

        for (int x = 0; x < cardinalityOfU; x++) {
            for (int z = 0; z < cardinalityOfU; z++) {
                double currentMax = 0.0;
                for (int y = 0; y < cardinalityOfU; y++) {
                    DomainElement xyElement = relationDomain.elementForIndex(x * cardinalityOfU + y);
                    DomainElement yzElement = relationDomain.elementForIndex(y * cardinalityOfU + z);
                    double minXYvsYZ = Math.min(relation.getValueAt(xyElement), relation.getValueAt(yzElement));
                    if (minXYvsYZ > currentMax)
                        currentMax = minXYvsYZ;
                }
                DomainElement xzElement = relationDomain.elementForIndex(x * cardinalityOfU + z);
                if (relation.getValueAt(xzElement) < currentMax)
                    return false;
            }
        }

        return true;
    }

    public static IFuzzySet compositionOfBinaryRelations(IFuzzySet relationA, IFuzzySet relationB) {

        IDomain relationADomain = relationA.getDomain();
        IDomain relationBDomain = relationB.getDomain();
        if (!relationADomain.getComponent(1).equals(relationBDomain.getComponent(0)))
            throw new InvalidParameterException("Matrice nije moguće množiti. Dimenzije moraju biti nxm prve i mxt da dobijemo dimenzije nxt!");
        MutableFuzzySet resultOfComposition = new MutableFuzzySet(new CompositeDomain(new ArrayList<>(Arrays.asList((SimpleDomain) relationADomain.getComponent(0), (SimpleDomain) relationBDomain.getComponent(1)))));

        int dimensionOfX = relationADomain.getComponent(0).getCardinality();
        int dimensionOfY = relationADomain.getComponent(1).getCardinality();
        int dimensionOfZ = relationBDomain.getComponent(1).getCardinality();

        for (int x = 0; x < dimensionOfX; x++) {
            for (int z = 0; z < dimensionOfZ; z++) {
                double currentMaxProduct = 0;
                for (int y = 0; y < dimensionOfY; y++) {
                    DomainElement xyRelationAElement = relationADomain.elementForIndex(x * dimensionOfY + y);
                    DomainElement yzRelationBElement = relationBDomain.elementForIndex(y * dimensionOfZ + z);
                    double minXYRelationAAndYZRelationB = Math.min(relationA.getValueAt(xyRelationAElement), relationB.getValueAt(yzRelationBElement));
                    if (minXYRelationAAndYZRelationB > currentMaxProduct)
                        currentMaxProduct = minXYRelationAAndYZRelationB;
                }
                DomainElement xzElement = resultOfComposition.getDomain().elementForIndex(x * dimensionOfZ + z);
                resultOfComposition.set(xzElement, currentMaxProduct);
            }
        }
        return resultOfComposition;

    }

    public static boolean isFuzzyEquivalence(IFuzzySet relation) {
        return Relations.isReflexive(relation) && Relations.isSymmetric(relation) && Relations.isMaxMinTransitive(relation);
    }


}
