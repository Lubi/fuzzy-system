package hr.fer.zemris.fuzzy;

import java.util.Arrays;

public class MutableFuzzySet implements IFuzzySet {
    private  double[] memberships;
    private IDomain domain;

    public MutableFuzzySet(IDomain domain) {
        this.domain = domain;
        this.memberships = new double[domain.getCardinality()];
        Arrays.fill(memberships, 0.0);
    }


    public MutableFuzzySet set(DomainElement domainElement, double newMembershipValue) {
        memberships[domain.indexOfElement(domainElement)] = newMembershipValue;
        return this;
    }

    @Override
    public IDomain getDomain() {
        return domain;
    }

    @Override
    public double getValueAt(DomainElement domainElement) {
        return memberships[domain.indexOfElement(domainElement)];
    }
}
