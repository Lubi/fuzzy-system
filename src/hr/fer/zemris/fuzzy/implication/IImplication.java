package hr.fer.zemris.fuzzy.implication;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

import java.util.List;

public interface IImplication {
    IIntUnaryFunction implicationOperator(ILanguageVariable function, List<Double> antecedentsMemberships);
}
