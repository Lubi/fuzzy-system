package hr.fer.zemris.fuzzy.implication;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

import java.util.List;

public class MinImplication  implements IImplication{
    @Override
    public IIntUnaryFunction implicationOperator(ILanguageVariable function, List<Double> antecedentsMemberships) {
        double minMembership = antecedentsMemberships.stream().mapToDouble(Double::doubleValue).min().orElseThrow();
        return x -> Math.min(minMembership, function.membership(x));
    }
}
