package hr.fer.zemris.fuzzy.implication;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;

import java.util.List;

public class ProductImplication implements IImplication{
    @Override
    public IIntUnaryFunction implicationOperator(ILanguageVariable function, List<Double> antecedentsMemberships) {
        double prod = antecedentsMemberships.stream().mapToDouble(Double::doubleValue).reduce(1, (a,b) -> a*b);
        return x ->  prod * function.membership(x);
    }
}
