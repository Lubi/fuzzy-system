package hr.fer.zemris.fuzzy.languageVariable;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.base.membershipFunctions.StandardFuzzySets;


public enum Velocity implements ILanguageVariable {
    LOW(StandardFuzzySets.lFunction(10, 20)),
    MEDIUM(StandardFuzzySets.lambdaFunction(15, 40, 60 )),
    LARGE(StandardFuzzySets.gammaFunction(50, 80));

    private final IIntUnaryFunction function;

    Velocity(IIntUnaryFunction function) {
        this.function = function;
    }

    @Override
    public double membership(int x) {
        return function.valueAt(x);
    }
}


