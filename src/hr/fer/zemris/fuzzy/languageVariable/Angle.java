package hr.fer.zemris.fuzzy.languageVariable;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.base.membershipFunctions.StandardFuzzySets;

public enum Angle implements ILanguageVariable {
    SHARPRIGHT(StandardFuzzySets.lFunction(-90, -60)),
    SLIGHTLYRIGHT(StandardFuzzySets.lambdaFunction(-70, -40, -10)),
    STRAIGHT(StandardFuzzySets.lambdaFunction(-20, 0, 20 )),
    SLIGHTLYLEFT(StandardFuzzySets.lambdaFunction(10, 40, 70)),
    SHARPLEFT(StandardFuzzySets.gammaFunction(60, 90));

    private final IIntUnaryFunction function;

    Angle(IIntUnaryFunction function) {
        this.function = function;
    }

    @Override
    public double membership(int x) {
        return function.valueAt(x);
    }
}
