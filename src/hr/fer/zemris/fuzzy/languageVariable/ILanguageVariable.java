package hr.fer.zemris.fuzzy.languageVariable;

import java.util.NoSuchElementException;

public interface ILanguageVariable {
    double membership(int x);
    static ILanguageVariable or(ILanguageVariable ... languageVariables){
        return  x -> {
            double currentMax = 0;
            for  (ILanguageVariable lngVar: languageVariables){
                double val = lngVar.membership(x);
                if  (val > currentMax)
                        currentMax = val;
            }
            return currentMax;
        };
    }
}
