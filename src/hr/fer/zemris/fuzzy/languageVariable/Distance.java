package hr.fer.zemris.fuzzy.languageVariable;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.base.membershipFunctions.StandardFuzzySets;

public enum Distance implements ILanguageVariable {
    CRITICALCLOSE(StandardFuzzySets.lFunction(20, 40)),
    CLOSE(StandardFuzzySets.lambdaFunction(30, 50, 70)),
    FAR(StandardFuzzySets.gammaFunction(70, 100));

    private final IIntUnaryFunction function;

    Distance(IIntUnaryFunction function) {
        this.function = function;
    }

    @Override
    public double membership(int x) {
        return function.valueAt(x);
    }
}
