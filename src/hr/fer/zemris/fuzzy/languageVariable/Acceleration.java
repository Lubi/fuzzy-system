package hr.fer.zemris.fuzzy.languageVariable;

import hr.fer.zemris.fuzzy.base.membershipFunctions.IIntUnaryFunction;
import hr.fer.zemris.fuzzy.base.membershipFunctions.StandardFuzzySets;

public enum Acceleration implements ILanguageVariable {
    LOW(StandardFuzzySets.lFunction(-60, -30)),
    MEDIUM(StandardFuzzySets.lambdaFunction(3, 7, 10)),
    LARGE(StandardFuzzySets.gammaFunction(30, 60));

    private final IIntUnaryFunction function;

    Acceleration(IIntUnaryFunction function) {
        this.function = function;
    }

    @Override
    public double membership(int x) {
        return function.valueAt(x);
    }
}
