package hr.fer.zemris.fuzzy.database;

import hr.fer.zemris.fuzzy.expression.CompositeExpression;
import hr.fer.zemris.fuzzy.expression.IExpression;
import hr.fer.zemris.fuzzy.expression.SimpleExpression;
import hr.fer.zemris.fuzzy.input.InputParamType;
import hr.fer.zemris.fuzzy.languageVariable.Angle;
import hr.fer.zemris.fuzzy.languageVariable.Distance;
import hr.fer.zemris.fuzzy.languageVariable.ILanguageVariable;
import hr.fer.zemris.fuzzy.rule.RuleBuilder;

import java.beans.Expression;
import java.util.ArrayList;
import java.util.List;

public class AngleDatabase {
    List<RuleBuilder> ruls = new ArrayList<>();

    public AngleDatabase() {
        makeRuls();
    }

    private void makeRuls() {
        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT, Distance.FAR),
                IExpression.is(InputParamType.LEFT45, Distance.FAR)
        ), Angle.SLIGHTLYLEFT));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.RIGHT, Distance.FAR),
                IExpression.is(InputParamType.RIGHT45, Distance.FAR)
        ), Angle.SLIGHTLYRIGHT));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE)),
                IExpression.is(InputParamType.LEFT45, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE))
        ), Angle.SHARPRIGHT)); /// oštro desno!!

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT45, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE))
        ), Angle.SHARPRIGHT)); /// oštro desno!!


        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.RIGHT, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE)),
                IExpression.is(InputParamType.RIGHT45, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE))
        ), Angle.SHARPLEFT)); /// oštro lijevo!!

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.RIGHT45, ILanguageVariable.or(Distance.CLOSE, Distance.CRITICALCLOSE))
        ), Angle.SHARPLEFT)); /// oštro lijevo!!

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT45, Distance.CLOSE),
                IExpression.is(InputParamType.RIGHT45, Distance.CLOSE)
        ), Angle.STRAIGHT));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT45, Distance.CRITICALCLOSE),
                IExpression.is(InputParamType.RIGHT45, Distance.CLOSE)
        ), Angle.SLIGHTLYRIGHT));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT45, Distance.CLOSE),
                IExpression.is(InputParamType.RIGHT45, Distance.CRITICALCLOSE)
        ), Angle.SLIGHTLYLEFT));

    }

    public List<RuleBuilder> getRuls() {
        return ruls;
    }
}