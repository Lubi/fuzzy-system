package hr.fer.zemris.fuzzy.database;

import hr.fer.zemris.fuzzy.expression.CompositeExpression;
import hr.fer.zemris.fuzzy.expression.IExpression;
import hr.fer.zemris.fuzzy.input.InputParamType;
import hr.fer.zemris.fuzzy.languageVariable.*;
import hr.fer.zemris.fuzzy.rule.RuleBuilder;

import java.util.ArrayList;
import java.util.List;

public class AccelerationDatabase {
    List<RuleBuilder> ruls = new ArrayList<>();

    public AccelerationDatabase() {
        makeRuls();
    }

    private void makeRuls(){
        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.VELOCITY,Velocity.LOW)
        ), Acceleration.LARGE));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.VELOCITY,Velocity.MEDIUM)
        ), Acceleration.MEDIUM));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.VELOCITY, Velocity.LARGE)
        ), Acceleration.LOW));


        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.LEFT45, Distance.CRITICALCLOSE),
                IExpression.is(InputParamType.VELOCITY, Velocity.LARGE)
        ), Acceleration.LOW));

        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.RIGHT45, Distance.CRITICALCLOSE),
                IExpression.is(InputParamType.VELOCITY, Velocity.LARGE)
        ), Acceleration.LOW));


        ruls.add(new RuleBuilder(new CompositeExpression(
                IExpression.is(InputParamType.RIGHT45, Distance.FAR),
                IExpression.is(InputParamType.LEFT45, Distance.FAR)
        ), Acceleration.LARGE));



    }

    public List<RuleBuilder> getRuls() {
        return ruls;
    }
}
